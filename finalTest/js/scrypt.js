/*   У вас есть массив книг books. Ваша задача создать из него два массива - goodBooks и badBooks.
    В первый входят все книни, автор которых - не Донцова. Во второй - книги, автор которых - Донуцова. Потому что:
    http://cdn01.ru/files/users/images/15/5c/155c1d353f38ac999440fee98d912f45.jpg
*/

let books = [ {author: "Скотт Бэккер", book: "Князь пустоты"},
    {author: "Донцова", book: "Старуха Кристи – отдыхает!"},
    {author: "Скотт Бэккер", book: "Тьма, что приходит прежде"},
    {author: "Скотт Бэккер", book: "Воин-пророк"},
    {author: "Скотт Бэккер", book: "Тысячекратная мысль"},
    {author: "Донцова", book: "Диета для трех поросят"},
    {author: "Скотт Бэккер", book: "Аспект-император"},
    {author: "Скотт Бэккер", book: "Око Судии"},
    {author: "Скотт Бэккер", book: "Воин Доброй удачи"},
    {author: "Донцова", book: "Идеальное тело Пятачка"},
    {author: "Скотт Бэккер", book: "Великая Ордалия"},
    {author: "Скотт Бэккер", book: "Нечестивый Консульт"},
    {author: "Скотт Бэккер", book: "Не-Бог"},
    {author: "Скотт Бэккер", book: "Истории о Злодеяниях"},
    {author: "Донцова", book: "Идеальное тело Пятачка"},
    {author: "Скотт Бэккер", book: "Ложное солнце"},
    {author: "Донцова", book: "Дед Снегур и Морозочка"},
    {author: "Скотт Бэккер", book: "Четыре Откровения Киниал’джина"},
    {author: "Донцова", book: "Инь, янь и всякая дрянь"},
    {author: "Скотт Бэккер", book: "Нож, что всем по руке"} ];

let badBooks =[];
let goodBooks = books.filter((item) => {
    if (item.author === "Донцова") {
        badBooks.push(item);
    } else {
        return item;
    }
});
console.log(goodBooks);
console.log(badBooks);

/*  Напишите программу, имитирующую снятие денег с кредитной карты. Для этого:
Cоздайте объект creditCard, с такими свойствами:
- количество денег на счету (изначально 10000);
- пин-код карты (4 любых цифры);
- количество оставшихся попыток ввода пин-кода (изначально 3, после каждого неправильного введенного кода уменьшается на 1. Если код введен верно -  их снова становится 3)
- статус карты (изначально - “active”, то есть работает. Если 3 раза подряд неправильно был введен пин-код, он меняется на “disabled”);

Добавьте в объект метод getCash, который будет принимать в себя 2 аргумента: пин-код в виде строки из 4х чисел, и желаемый объем денег, которые владелец карты хочет снять. Метод будет работать так:
- если статус карты равен "disabled", он возвращает сообщение: “Ваша карта заблокирована, обратитесь в банк для ее разблокировки”;
- если пин-код не верен, он уменьшает количество попыток ввести пин-код на 1, и если после этого количество попыток ввода пин-кода
 больше 0 - возвращает строку “Неправильный пин-код! Попробуйте пожалуйста снова!”
- если пин-код неверен, и количество попыток после уменьшения на 1 стало 0, то он меняет значение свойства СтатусКарты на
"disabled"  возвращает строку “Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в банк для ее разблокировки”;
- если пин-код верен, то:
    - если требуемая сумма меньше текущей суммы на счету, уменьшить сумму на счету на желаемую (то есть было 10000 например,
     человек хочет снять 4000 - осталось 6000), установить счетчик попыток ввести пин-код снова на 3  и вернуть сообщение:
      “Получите ваши - сумма, которую человек хочет снять-”.
    - если требуемая сумма превышает остаток на счету, то установить счетчик попыток ввести пин-код снова на 3
     и вернуть сообщение: “К сожалению, на вашем счету недостаточно средств”.

Требуемую сумму, а также пин-код нужно ввести в соответствующие поля (input) внизу, выполнения метода getCash повесить на кнопку “Получить наличные”,
 а результат выполнения функции вывести в p с id cash-request-result.
Изящные решения с использованием массивов ответов приветствуются.
 */
let pinCode = document.getElementById("pin-code");
let amountMoney = document.getElementById("money-sum");
let info = document.getElementById("cash-request-result");
const createCreditCard = () => {
    const creditCard = {};
    creditCard.amountMoney = 10000;
    creditCard.pinCode = 1234;
    creditCard.pinInputCount = 3;
    creditCard.cardStatus = "disabled";
    creditCard.getCash = function (pinCode, amountOfMoney) {
        if (this.cardStatus === "active") {
            info.innerText = "Ваша карта заблокирована, обратитесь в банк для ее разблокировки";
            console.log("Ваша карта заблокирована, обратитесь в банк для ее разблокировки");
            return;
        }
        if (this.pinCode !== pinCode) {
            this.pinInputCount--;
            if (this.pinInputCount > 0) {
                info.innerText = "Неправильный пин-код! Попробуйте пожалуйста снова!";
            } else {
                this.cardStatus = "disable";
                info.innerText = "Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в банк для ее разблокировки";
            }
        } else {
            if (this.amountMoney >= amountOfMoney) {
                this.pinInputCount = 3;
                this.amountMoney -= amountOfMoney;
                info.innerText = "Получите ваши - " + amountOfMoney;
            } else {
                this.pinInputCount = 3;
                info.innerText = "К сожалению, на вашем счету недостаточно средств";
            }
        }
    }
    return creditCard;
}
const card = createCreditCard();
let btn = document.getElementById("get-cash");
btn.addEventListener("click", (event) => {
    card.getCash(+(pinCode.value), +(amountMoney.value));
})

/*   У вас есть массив строк, каждая строка - один продукт, который входит в состав английского завтрака.
 Напишите код, который работает так: если в состав завтрака входит больше 4х продуктов, то остальные переносятся на обед.*/

const englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы", "жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];
let dinner =[];

const checkBreakfast = (arrBreakfast) => {
    if (arrBreakfast.length > 4) {
        dinner = arrBreakfast.slice(3);
        arrBreakfast.length = 4;
    }
    return dinner;
}

console.log(checkBreakfast(englishBreakfast));
console.log(englishBreakfast);

/* *У вас есть массив объектов (женихи из Японии), каждый объект - характеристики потенциального жениха..
Создайте из  них другой массив -  "завидные женихи". Завидным считается парень с ростом  выше 170 см или с зарплатой
 больше 100 000 долларов в год.
Использование стрелочных функций приветствуется.
*/

const japanMen = [{name: "Акэти Мицихура", height: 180, salary: 20000},
    {name: "Санада Юкимура", height: 169, salary: 11000},
    {name: "Ода Нобунага", height: 165, salary: 200000},
    {name: "Уэсуги Кэнсин", height: 160, salary: 25000},
    {name: "Такэда Сингэн", height: 165, salary: 80000},
    {name: "Мори Мотонари", height: 185, salary: 75000},
    {name: "Датэ Масамунэ", height: 180, salary: 750000}];

/*В результате у вас должен повится новый массив japanBestMen с такими значениями:

const japanBestMen = [{name: "Акэти Мицихура", height: 180, salary: 20000},
 {name: "Ода Нобунага", height: 165, salary: 200000},
 {name: "Мори Мотонари", height: 185, salary: 75000},
  {name: "Датэ Масамунэ", height: 180, salary: 750000}];
*/
const filterForBestMan = (arrMan) => {
   return  arrMan.filter((item) => {
        if (item.height > 170 || item.salary > 100000) {
            return item;
        }
    })
}

const  japanBestMen = filterForBestMan(japanMen);
console.log(japanBestMen);

/*
* Представьте себе ситуацию - вам нужно оценить, является ли комментарий пользователя на сайте недопустимым с лексической
* точки зрения (то есть содержит слова, которые нельзя употреблят, например: “огромные выигрыши в нашем казино набирай в браузере …. ”).
*  Логично, что для этого нужно проверить содержит ли комментарий пользователя на сайте слишком много спам-слов (например,
* 1-2 слова “бесплатно” - это ок, но 3 и больше уже явный признак спама ). Для этого вам нужно написать функцию (именно функцию)
* isSpam, которая будет принимать 3 аргумента:
    - строку, которую ввел пользователь;
    - слово, переизбыток которого считается спамом (например, “выигрыш”);
    - количество повторений слова, которое считается спамом (например, 3 - то есть если слово “выигрыш” встречается в
    * строке 1-2 раза, это не спам, а если 3 и больше - спам).
    И возвращать функция isSpam должна true, если в переданной строке спам-слово встречается указанное или больше число раз, и false - если нет.
    При этом и комментарий, и спм-слово, нужно ввести в поля ввода, которые написаны ниже (input) и повесить выполнение
    * функции на клик на кнопке “Проверить на спам”. Результат выполнения функции вывести в p с id spam-check-result. */

let comment = document.getElementById("comment");
let spamWord = document.getElementById("spam-word");
let fieldInfo  = document.getElementById("spam-check-result");

const isSpam = (comment, spamWord, count) => {
    let arrWord = [];
    arrWord = comment.split(" ");
    arrWord.forEach((item) => {
        if (item.toLowerCase() === spamWord.toLowerCase()) {
            count--;
        }
    })
    return count < 0;
}
document.getElementById("send-comment").addEventListener("click", () => {
    fieldInfo.innerText = isSpam(comment.value, spamWord.value, 3);
})

